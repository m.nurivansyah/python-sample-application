.PHONY: bootstrap clean lint test
.DEFAULT_GOAL := test

test: clean lint
	@py.test test/ --cov app.py -s

lint:
	@flake8 .

clean:
	@find . -type f -name '*.pyc' -delete

bootstrap:
	@pip install -r requirements.txt
	@pip install -r requirements-test.txt
	@python setup.py develop

PROJECT_ID=devops-test-351116
REGION=asia-southeast2
IMAGE_TAG=python-sample-application
CI_COMMIT_SHORT_SHA?=latest
LOCAL_TAG=$(IMAGE_TAG):$(CI_COMMIT_SHORT_SHA)
REMOTE_TAG=gcr.io/$(PROJECT_ID)/$(LOCAL_TAG)
VERSION?=latest
VERSION_LOCAL_TAG=$(IMAGE_TAG):$(VERSION)
VERSION_REMOTE_TAG=gcr.io/$(PROJECT_ID)/$(VERSION_LOCAL_TAG)

docker-images:
	docker images | grep $(IMAGE_TAG)

docker-build:
	docker build . --file ./build/package/Dockerfile --tag  $(LOCAL_TAG)

docker-tag:
	docker tag $(LOCAL_TAG) $(REMOTE_TAG)

docker-pull:
	docker pull $(REMOTE_TAG)

docker-push:
	docker push $(REMOTE_TAG)

docker-tag-version:
	docker tag $(REMOTE_TAG) $(VERSION_REMOTE_TAG)

docker-push-version:
	docker push $(VERSION_REMOTE_TAG)

### Infrastructure

gcloud-auth:
	gcloud auth activate-service-account --key-file=${GOOGLE_APPLICATION_CREDENTIALS}

create-terraform-backend-bucket:
	gsutil mb -p ${PROJECT_ID} gs://${PROJECT_ID}-terraform

###

check-env:
ifndef ENV
	$(error Please set ENV=[staging|production])
endif

define get-secret
$(shell gcloud secrets versions access latest --secret=$(1) --project=$(PROJECT_ID))
endef

###

terraform-create-workspace: check-env
	cd terraform && \
		terraform workspace new ${ENV}

terraform-init: check-env
	cd terraform && \
		terraform workspace select ${ENV} && \
		terraform init

terraform-format: check-env
	cd terraform && \
		terraform workspace select ${ENV} && \
		terraform fmt

TF_ACTION?=plan
terraform-action: check-env
	@cd terraform && \
		terraform workspace select ${ENV} && \
		export TF_VAR_axiatalabs_uber_client_id=$(call get-secret,uber_client_id_${ENV}) &&\
		export TF_VAR_axiatalabs_uber_client_secret=$(call get-secret,uber_client_secret_${ENV}) &&\
		terraform ${TF_ACTION} \
		-var-file="./environments/common.tfvars" \
		-var-file="./environments/${ENV}/config.tfvars"
