output "app_service_url" {
  value = google_cloud_run_service.axiatalabs-service.status[0].url
}
