
resource "google_cloud_run_service" "axiatalabs-service" {
  name     = "axiatalabs-service${terraform.workspace == "production" ? "" : "-${terraform.workspace}"}"
  location = var.region

  template {
    spec {
      containers {
        image = var.axiatalabs_container_image
        ports {
          container_port = var.axiatalabs_container_port
        }
        resources {
          limits = {
            "cpu"    = "1000m"
            "memory" = "256Mi"
          }
        }
        env {
          name  = "UBER_CLIENT_ID"
          value = var.axiatalabs_uber_client_id
        }
        env {
          name  = "UBER_CLIENT_SECRET"
          value = var.axiatalabs_uber_client_secret
        }
      }
    }
    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale" = var.axiatalabs_max_instance
        "run.googleapis.com/client-name"   = "terraform"
        "run.googleapis.com/sandbox"       = "gvisor"
      }
    }
  }

  autogenerate_revision_name = true
}

data "google_iam_policy" "axiatalabs-policy" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "axiatalabs-service-policy" {
  location = google_cloud_run_service.axiatalabs-service.location
  project  = google_cloud_run_service.axiatalabs-service.project
  service  = google_cloud_run_service.axiatalabs-service.name

  policy_data = data.google_iam_policy.axiatalabs-policy.policy_data
}
