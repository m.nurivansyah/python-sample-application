# GLOBAL

variable "project" {
  type = string
}

variable "project_number" {
  type = string
}

variable "region" {
  type = string
}

variable "zone" {
  type = string
}

# APP

variable "axiatalabs_container_image" {
  type = string
}

variable "axiatalabs_container_port" {
  type = number
}

variable "axiatalabs_max_instance" {
  type = number
}

variable "axiatalabs_uber_client_id" {
  type      = string
  sensitive = true
}

variable "axiatalabs_uber_client_secret" {
  type      = string
  sensitive = true
}
