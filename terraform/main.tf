terraform {
  backend "gcs" {
    bucket = "devops-test-351116-terraform"
    prefix = "/state/axiatalabs/"
  }
}

provider "google" {
  project = var.project
  region  = var.region
  zone    = var.zone
}
